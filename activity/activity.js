/* Instructions:

	Create a function which will be able to add two numbers.
		-Number must be provided as arguments.
		-Display the result of the addition in our console.

	Create a function which will be able to subtract two numbers.
		-Number must be provided as arguments.
		-Display the result of the subtraction in our console.

	Create a function which will be able to multiply two numbers.
		-Number must be provided as arguments.
		-Return the result of the multiplication.

	Create a new variable called product.
		-This product should be able to receive the result of multiplication.

	Log the value of product variable in the console.
*/

function addNum(num1, num2){
	console.log(num1 + num2);
};
addNum(3,9);

function minusNum(num1, num2){
	console.log(num1 - num2);
};
minusNum(8,5);

function multiNum(num1, num2){
	return num1 * num2;
};
let product = multiNum(7,6);
console.log(product);















